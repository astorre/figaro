# frozen_string_literal: true
class User < ApplicationRecord
  has_many :orders, dependent: :destroy

  validates :login, presence: true, length: { maximum: 255 },
            uniqueness: true
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }, on: :create

  # def authenticated?(attribute, token)
  #   digest = send("#{attribute}_digest")
  #   BCrypt::Password.new(digest).is_password?(token)
  # end
end

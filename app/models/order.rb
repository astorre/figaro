class BagetsValidator < ActiveModel::Validator
  def validate(record)
    if record.send(options[:field]).empty?
      record.errors[:base] << 'Материалы не выбраны!'
    end
  end
end

class Order < ApplicationRecord
  belongs_to :user

  validates_with BagetsValidator, field: :bagets
end

# frozen_string_literal: true
module ApplicationHelper
  def bootstrap_class_for(flash_type)
    case flash_type
    when 'success' then 'alert-success' # Green
    when 'error'   then 'alert-danger'  # Red
    when 'alert'   then 'alert-warning' # Yellow
    when 'notice'  then 'alert-info'    # Blue
    else flash_type.to_s
    end
  end

  def fa_icon_class_for(flash_type)
    case flash_type
    when 'success' then 'fa-check'
    when 'error'   then 'fa-remove'
    when 'alert'   then 'fa-warning'
    when 'notice'  then 'fa-info'
    else flash_type.to_s
    end
  end

  def alert_topic_for(flash_type)
    case flash_type
    when 'success' then 'Готово!'
    when 'error'   then 'Ошибка!'
    when 'alert'   then 'Внимание!'
    when 'notice'  then 'Сообщение!'
    else flash_type.to_s
    end
  end
end

# frozen_string_literal: true
class SessionsController < ApplicationController
  def new
    redirect_to orders_path if logged_in?
  end

  def create
    user = User.find_by(login: params[:session][:login].downcase)
    if user && user.authenticate(params[:session][:password])
      log_in user
      redirect_back_or orders_path
    else
      flash[:error] = 'Неверный логин или пароль'
      redirect_to root_path
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_path
  end
end

# frozen_string_literal: true
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include ApplicationHelper
  include SessionsHelper

  private

  def logged_in_user
    return if logged_in?
    store_location
    flash[:error] = 'Пожалуйста, авторизуйтесь!'
    redirect_to root_path
  end
end

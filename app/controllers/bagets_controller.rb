# frozen_string_literal: true
class BagetsController < ApplicationController
  before_action :logged_in_user

  def index
    @bagets = Baget.order(updated_at: :desc)
  end

  def create
    baget = Baget.new(baget_params)
    if baget.save
      respond_to do |format|
        format.js { render 'create', locals: { id: baget.id,
                                               code: baget.code,
                                               name: baget.name,
                                               retail_price: baget.retail_price,
                                               units: baget.units,
                                               created_at: baget.created_at.strftime('%d.%m.%Y %k:%M:%S'),
                                               updated_at: baget.updated_at.strftime('%d.%m.%Y %k:%M:%S') } }
      end
    else
      @error = baget.errors
      respond_to do |format|
        format.js { render 'shared/error' }
      end
    end
  end

  private

  def baget_params
    params.require(:baget).permit(:code, :name, :retail_price, :units)
  end
end

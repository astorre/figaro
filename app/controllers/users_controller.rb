# frozen_string_literal: true
class UsersController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:success] = 'Пользователь создан'
      redirect_to new_user_path
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @user.update_attributes(edit_user_params)
      flash[:success] = 'Профиль успешно обновлён'
      redirect_to users_path
    else
      render :edit
    end
  end

  def destroy
    @user = User.find_by(id: params[:id])
    @user.destroy
  end

  private

  def user_params
    params.require(:user).permit(:login,
                                 :name,
                                 :phone,
                                 :office,
                                 :password,
                                 :password_confirmation)
  end
end

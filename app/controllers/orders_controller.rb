# frozen_string_literal: true
class Order_obj
  attr_reader :code, :name, :mustaches, :note, :size, :units, :quantity, :consumption, :retail_price, :sum

  def initialize(code, name, mustaches, note, size, units, quantity, consumption, retail_price, sum)
    @code = code
    @name = name
    @mustaches = mustaches
    @note = note
    @size = size
    @units = units
    @quantity = quantity
    @consumption = consumption
    @retail_price = retail_price
    @sum = sum
  end
end

class OrdersController < ApplicationController
  before_action :logged_in_user

  def index
    @orders = current_user.orders.includes(:user).order(updated_at: :desc)
  end

  def create
    order = Order.new(order_params)
    bagets = Baget.select(:code, :name, :mustaches, :retail_price, :units)
                  .where(id: params.dig(:order_bagets, :bagets))
    order_bagets = bagets.map { |e| {
                                      code: e.code,
                                      name: e.name,
                                      note: 'примечание',
                                      size: '',
                                      mustaches: e.mustaches,
                                      retail_price: e.retail_price,
                                      units: e.units,
                                      quantity: 1,
                                      consumption: 1.0
                                    } }
    order.bagets = order_bagets
    order.number = loop do
      token = SecureRandom.hex(3)
      break token unless Order.exists?(number: token)
    end
    order.user = current_user
    if order.save
      respond_to do |format|
        format.js { render 'create', locals: { id: order.id,
                                               number: order.number,
                                               client: order.client,
                                               login: order.user.login,
                                               created_at: order.created_at.strftime('%d.%m.%Y %k:%M:%S'),
                                               updated_at: order.updated_at.strftime('%d.%m.%Y %k:%M:%S') } }
      end
    else
      @error = order.errors.full_messages.join(', ')
      respond_to do |format|
        format.js { render 'shared/error' }
      end
    end
  end

  def show
    @order = current_user.orders.find(params[:id])
    respond_to do |format|
      format.html
      format.xls { send_data xls_grouper(params[:id]), filename: "order#{params[:id]}.xls" }
    end
  end

  private

  def order_params
    params.require(:order).permit(:client)
  end

  def xls_grouper(order_id)
    @order = current_user.orders.find(order_id)
    array = []
    @order.bagets.each do |baget|
      array << Order_obj.new(baget.fetch('code'),
                             baget.fetch('name'),
                             baget.fetch('mustaches'),
                             baget.fetch('note'),
                             baget.fetch('size'),
                             baget.fetch('units'),
                             baget.fetch('quantity'),
                             baget.fetch('consumption'),
                             baget.fetch('retail_price'),
                             baget.fetch('retail_price') * baget.fetch('quantity') * baget.fetch('consumption'))
    end
    array.to_xls(
        cell_format: {
          weight:           :bold,
          bottom_color:     :green,
          left:             :dotted,
          right:            :dashed,
          top:              :dotted,
          bottom:           :dashed,
          horizontal_align: :left,
          font: Spreadsheet::Font.new('Courier New', color: :blue)
        },
        header_format: {
          weight: :bold,
          color:  :black,
          right:  :dashed,
          bottom: :double,
          diagonal_color: :red,
          pattern_fg_color: :red,
          pattern_bg_color: :border,
          cross_down: true
        },
        columns: [:code,
                  :name,
                  :mustaches,
                  :note,
                  :size,
                  :units,
                  :quantity,
                  :consumption,
                  :retail_price,
                  :sum],
        headers: %w(Артикул Наименование Усы Примечание Размер Ед.изм. Кол. Расход Цена Сумма)
    ).force_encoding('UTF-8')
  end
end

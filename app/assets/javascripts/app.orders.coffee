document.addEventListener "turbolinks:load", ->
  return unless $(".orders.index").length > 0

  dualBox = $('select[name="order_bagets[bagets][]"]').bootstrapDualListbox {
    filterTextClear: 'Показать все'
    filterPlaceHolder: 'Фильтр'
    infoTextEmpty: 'Список пуст'
    infoText: 'Выбрано {0}'
  }
  # dualBox.refresh()
  $('.moveall i').removeClass().addClass('fa fa-arrow-right')
  $('.removeall i').removeClass().addClass('fa fa-arrow-left')

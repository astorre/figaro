Rails.application.routes.draw do
  resources :orders
  resources :bagets
  root 'sessions#new'
  resources :users
  post 'login' => 'sessions#create'
  delete 'logout' => 'sessions#destroy'
end

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170127151938) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "bagets", force: :cascade do |t|
    t.string   "code",                                                  null: false
    t.string   "name",                                                  null: false
    t.decimal  "retail_price", precision: 15, scale: 2, default: "0.0", null: false
    t.string   "units",                                                 null: false
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.string   "mustaches"
    t.index ["code"], name: "index_bagets_on_code", using: :btree
  end

  create_table "orders", force: :cascade do |t|
    t.string   "number",                    null: false
    t.string   "client",                    null: false
    t.integer  "user_id",                   null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.jsonb    "bagets",     default: "{}", null: false
    t.index ["bagets"], name: "index_orders_on_bagets", using: :gin
    t.index ["user_id"], name: "index_orders_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "login",                           null: false
    t.string   "password_digest",                 null: false
    t.string   "name",                            null: false
    t.string   "phone"
    t.string   "office",                          null: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.boolean  "admin",           default: false
    t.index ["login"], name: "index_users_on_login", using: :btree
  end

  add_foreign_key "orders", "users"
end

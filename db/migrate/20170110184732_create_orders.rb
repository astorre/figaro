# frozen_string_literal: true
class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.string :number, null: false
      t.string :client, null: false
      t.references :user, index: true, foreign_key: true, null: false

      t.timestamps
    end
  end
end

# frozen_string_literal: true
class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :login, index: true, null: false
      t.string :password_digest, null: false
      t.string :name, null: false
      t.string :phone
      t.string :office, null: false

      t.timestamps
    end
  end
end

# frozen_string_literal: true
class AddMustachesToBagets < ActiveRecord::Migration[5.0]
  def change
    add_column :bagets, :mustaches, :string
  end
end

# frozen_string_literal: true
class CreateBagets < ActiveRecord::Migration[5.0]
  def change
    create_table :bagets do |t|
      t.string :code, index: true, null: false
      t.string :name, null: false
      t.decimal :retail_price, precision: 15, scale: 2, default: 0.0, null: false
      t.string :units, null: false

      t.timestamps
    end
  end
end

# frozen_string_literal: true
class AddBagetsToOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :bagets, :jsonb, null: false, default: '{}'
    add_index  :orders, :bagets, using: :gin
  end
end

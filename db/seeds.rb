# frozen_string_literal: true

# Users
User.create(login: 'admin', name: 'Vasya', office: 'Leninsky prospect', password: 'password')
User.create(login: 'staff1', name: 'Atos', office: 'Dzerzhinsky prospect', password: 'password')
User.create(login: 'staff2', name: 'Partos', office: 'Volgogradsky prospect', password: 'password')
User.create(login: 'staff3', name: 'Aramis', office: 'Timiryazevsky prospect', password: 'password')

# Bagets
Baget.find_or_create_by(code: 'we65e6ew6f', name: 'Рама №1', retail_price: 557.31, units: 'м')
Baget.find_or_create_by(code: 'jshdf7s6f', name: 'Картон №1', retail_price: 98.4, units: 'м2')
Baget.find_or_create_by(code: 'hgqf2gc', name: 'Рама №3', retail_price: 34.07, units: 'пм')
Baget.find_or_create_by(code: 'ldt09adtfqy2eg', name: 'Рама №6', retail_price: 12.45, units: 'м')

# Orders
3.times do
  Order.create(number: Faker::Number.number(10), client: Faker::Name.name, user_id: 4)
end
3.times do
  Order.create(number: Faker::Number.number(10), client: Faker::Name.name, user_id: 5)
end
3.times do
  Order.create(number: Faker::Number.number(10), client: Faker::Name.name, user_id: 6)
end
